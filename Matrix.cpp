#include "Matrix.h"

#include <iostream>

Matrix::Matrix(const std::vector<double>& values,
	size_t rows,
	size_t cols) :
	m_rows(rows),
	m_cols(cols),
	m_values(values)
{
}

std::vector<double> Matrix::GetData() const
{
	return m_values;
}

size_t Matrix::GetCols() const
{
	return m_cols;
}

size_t Matrix::GetRows() const
{
	return m_rows;
}

double& Matrix::at(size_t indexRow, size_t indexColumn)
{
	return m_values[indexRow * m_cols + indexColumn];
}

void Matrix::Print() 
{
	for (size_t indexRow = 0; indexRow < m_rows; ++indexRow)
	{
		for (size_t indexCol = 0; indexCol < m_cols; ++indexCol)
			std::cout << at(indexRow, indexCol) << " ";
		std::cout << std::endl;
	}
}
