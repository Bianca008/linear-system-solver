#include "SphericalHarmonics.h"
#include "Point3D.h"
#include "PolarCoordinates.h"

#include <cmath>
#include <iostream>

double SphericalHarmonics::GetRe(int l, int m, double theta, double phi) const
{
	return std::sph_legendre(l, m, theta) * cos(m * phi);
}

double SphericalHarmonics::GetIm(int l, int m, double theta, double phi) const
{
	return std::sph_legendre(l, m, theta) * sin(m * phi);
}

std::complex<double> SphericalHarmonics::ComputeSystemMatrixElement(
	const std::vector<std::tuple<double, double, double>>& pointsInPolarCoordinates,
	int p,
	int q,
	int l,
	int m)
{
	static size_t pos = 0;
	std::vector<std::complex<double>> vecToReatain;
	std::complex<double> sum(0, 0);

	for (size_t index = 0; index < pointsInPolarCoordinates.size(); ++index)
	{
		std::complex<double> first(GetRe(p, q, std::get<1>(pointsInPolarCoordinates[index]), std::get<2>(pointsInPolarCoordinates[index])), GetIm(p, q, std::get<1>(pointsInPolarCoordinates[index]), std::get<2>(pointsInPolarCoordinates[index])));
		std::complex<double> second(GetRe(l, m, std::get<1>(pointsInPolarCoordinates[index]), std::get<2>(pointsInPolarCoordinates[index])), GetIm(l, m, std::get<1>(pointsInPolarCoordinates[index]), std::get<2>(pointsInPolarCoordinates[index])));
		
		sum += first * second;
		if (p == 0 && q == 0)
			vecToReatain.emplace_back(second);
	}
	m_commonMatrix.emplace_back(vecToReatain);
	
	return sum;
}

std::complex<double> SphericalHarmonics::ComputeSystemVectorElement(
	const std::vector<std::tuple<double, double, double>>& pointsInPolarCoordinates,
	int posVec,
	int l,
	int m) const
{
	std::complex<double> sum(0, 0);
	for (size_t index = 0; index < pointsInPolarCoordinates.size(); ++index)
		sum += std::get<0>(pointsInPolarCoordinates[index]) * m_commonMatrix[posVec][index];

	return sum;
}

std::vector<std::complex<double>> SphericalHarmonics::ComputeSystemMatrix(
	const std::vector<std::tuple<double, double, double>>& pointsInPolarCoordinates,
	uint16_t lmax)
{
	std::vector<std::complex<double>> values;
	size_t d = pow(lmax + 1, 2);
	int l = 0;
	int m = 0;
	int p = 0;
	int q = 0;

	for (size_t indexRow = 0; indexRow < d; indexRow++)
		for (size_t indexColumn = 0; indexColumn < d; indexColumn++)
			values.emplace_back(0, 0);

	for (size_t indexRow = 0; indexRow < d; indexRow++)
		for (size_t indexColumn = indexRow; indexColumn < d; indexColumn++)
		{
			values[indexRow * d + indexColumn] = ComputeSystemMatrixElement(pointsInPolarCoordinates, l, m, p, q);
			values[indexColumn * d + indexRow] = values[indexRow * d + indexColumn];
			if (p < lmax)
			{
				if (p == q)
				{
					p++;
					q = -p;
				}
				else q++;
			}
			else
				if (p == q)
					if (l == m)
					{
						l++;
						m = -l;
						p = l;
						q = m;
					}
					else {
						m++;
						p = l;
						q = m;
					}
				else q++;
		}

	return values;
}

std::vector<std::complex<double>> SphericalHarmonics::ComputeSystemVector(
	const std::vector<std::tuple<double, double, double>>& pointsInPolarCoordinates,
	uint16_t lmax) const
{
	std::vector<std::complex<double>> values;

	for (int l = 0; l <= lmax; l++)
		for (int t = -l; t <= l; t++)
			values.emplace_back(0, 0);

	size_t indexVec = 0;

	for (int l = 0; l <= lmax; l++)
		for (int t = -l; t <= l; t++)
		{
			values[l * l + l + t] = ComputeSystemVectorElement(pointsInPolarCoordinates, indexVec, l, t);
			++indexVec;
		}


	return values;
}

//ReA -ImA   Rex   Reb
//ImA  ReA   Imx   Imb  

//   A    *   x  =  b     

//   A = ReA + iImA; x = Rex + iImx; b = Reb + iImb

//construiesc sirul numerelor reale ce reprezinta matricea specificata 
//mai sus si care are dimensiunea (2*(lmax + 1)*(lmax + 1))^2
std::vector<double> SphericalHarmonics::RealSystemMatrix(std::vector<std::complex<double>> v, size_t lmax) const
{
	//dim= (lmax+1)^2
	size_t dim = std::pow(lmax + 1, 2);
	std::vector<double> w;
	for (size_t indexRow = 0; indexRow < 2 * dim; ++indexRow)
		for (size_t indexColumn = 0; indexColumn < 2 * dim; ++indexColumn)
			w.emplace_back(0.0);

	for (size_t indexRow = 0; indexRow < dim; indexRow++)
		for (size_t indexColumn = 0; indexColumn < dim; indexColumn++)
		{
			w[2 * indexRow * dim + indexColumn] = v[indexRow * dim + indexColumn].real();
			w[2 * dim * indexRow + dim + indexColumn] = -v[indexRow * dim + indexColumn].imag();
			w[2 * dim * (indexRow + dim) + indexColumn] = v[indexRow * dim + indexColumn].imag();
			w[2 * dim * (indexRow + dim) + indexColumn + dim] = v[indexRow * dim + indexColumn].real();
		}

	return w;
}

//construiesc sirul numerelor reale ce reprezinta vectorul termenilor liberi
std::vector<double> SphericalHarmonics::RealSystemVector(std::vector<std::complex<double>> v) const
{
	size_t dim = v.size();
	std::vector<double> w;
	for (size_t index = 0; index < 2 * dim; ++index)
		w.emplace_back(0.0);

	for (size_t index = 0; index < dim; index++)
	{
		w[index] = v[index].real();
		w[dim + index] = v[index].imag();
	}

	return w;
}

double prod(double initial, double final, double step)
{
	double p = 1;
	for (double v = initial; v <= final; v += step)
		p *= v;
	return p;
}

int comb(int n, int k)
{
	return (int)prod(n - k + 1, n, 1) / (int)prod(2, k, 1);
}

std::complex<double> SphericalHarmonics::GetComplexCoeff(int l, int m, double theta, double phi) const
{
	double g = GetCoeff(l, m, theta);
	std::complex<double> c(g * cos(m * phi), g * sin(m * phi));
	return c;
}

double SphericalHarmonics::GetCoeff(int l, int m, double theta) const
{
	constexpr double PI = 3.14159265358979323846;

	double v_0 = .5 * sqrt((2 * l + 1) / PI * prod(2, l - m, 1) / prod(2, l + m, 1));
	double v_1 = 1 / (double)(1 << l) / prod(2, l, 1);
	double v_2 = pow(abs(sin(theta)), abs(m));
	double v_3 = 0;
	for (int k = 0; k <= l; k++)
	{
		v_3 += ((l - k) % 2 ? -1 : 1) * comb(l, k) * prod(2 * k - l - m + 1, 2 * k, 1) * pow(cos(theta), 2 * k - l - m);
		//cout << "#" << l<< " " << m << " " << k << " " << theta << " " << pow(cos(theta), 2 * k - l - m) << endl;
	}
	return (abs(m) % 2 ? -1 : 1) * v_0 * v_1 * v_2 * v_3;
}
