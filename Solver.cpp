#include "Matrix.h"
#include "MatrixOperations.h"
#include "PolarCoordinates.h"
#include "Point3D.h"
#include "TransformMatrixIntoBinaryFile.h"
#include "SphericalHarmonics.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <time.h>

std::vector<Point3D> ReadMatrix(const std::string& fileName)
{
	std::ifstream file(fileName);

	std::vector<Point3D> points;

	size_t n;
	file >> n;

	for (size_t index = 0; index < n; ++index)
	{
		float x, y, z;
		file >> x >> y >> z;
		Point3D point(x, y, z);
		points.emplace_back(point);
	}

	return points;
}

void RunAlgorithm()
{
	SphericalHarmonics sh;
	std::vector<Point3D> points = ReadMatrix("duckPoints.txt");
	std::vector<std::tuple<double, double, double>> pointsInPolarCoordinates;

	for (size_t index = 0; index < points.size(); ++index)
		pointsInPolarCoordinates.emplace_back(PolarCoordinates::GetPolarCoordinates(points[index]));

	constexpr uint8_t lmax = 10;
	clock_t t;
	std::cout << "start compute matrix and vector\n";
	t = clock();

	std::vector<std::complex<double>> matrix = sh.ComputeSystemMatrix(pointsInPolarCoordinates, lmax);
	std::vector<std::complex<double>> vector = sh.ComputeSystemVector(pointsInPolarCoordinates, lmax);

	std::vector<double> realMatrix = sh.RealSystemMatrix(matrix, lmax);
	std::vector<double> realVector = sh.RealSystemVector(vector);

	t = clock() - t;
	std::cout << ((float)t / CLOCKS_PER_SEC) << " seconds";
	std::cout << "\nend\n";

	MatrixOperations matrixOperations;
	//A
	Matrix matrixValues(realMatrix, 2 * std::pow(lmax + 1, 2), 2 * std::pow(lmax + 1, 2));
	//b
	Matrix vectorValues(realVector, 2 * std::pow(lmax + 1, 2), 1);

	std::cout << "\nstart solve system\n";
	t = clock();

	Matrix result = matrixOperations.SolveSystem(matrixValues, vectorValues);

	t = clock() - t;
	std::cout << ((float)t / CLOCKS_PER_SEC) << " seconds";
	std::cout << "\nend\n";

	std::vector<std::complex<double>> clm;
	std::vector<double> resultValues = result.GetData();
	size_t dim = resultValues.size() / 2;

	for (size_t index = 0; index < dim; ++index)
		clm.push_back(std::complex<double>(0, 0));

	for (size_t index = 0; index < dim; ++index)
	{
		clm[index] = std::complex<double>(resultValues[index], resultValues[dim + index]);
	}

	std::ofstream ff("resultPoints100.txt");
	std::cout << std::endl;
	for (int i = 0; i < points.size(); i++)
	{
		std::complex<double> r(0, 0);
		Point3D shy(points[i].x, points[i].y, points[i].z);
		const auto& [lung, theta, phi] = PolarCoordinates::GetPolarCoordinates(shy);
		for (int l = 0; l <= lmax; l++)
			for (int m = -l; m <= l; m++)
			{
				std::complex<double> complexPoint(sh.GetRe(l, m, theta, phi), sh.GetIm(l, m, theta, phi));
				r += clm[l * l + l + m] * complexPoint;
			}

		ff << "v ";
		ff << (r * cos(phi) * sin(theta)).real() << " ";
		ff << (r * sin(phi) * sin(theta)).real() << " ";
		ff << (r * cos(theta)).real() << std::endl;
	}
	ff.close();
}

int main()
{
	RunAlgorithm();

	//1. De rezolvat sistemul Ax = b DONE
	//2. De verificat ca avem calcule corecte(se calculeaza pt fiecare punct theta si phi si se 
	//calculeaza punctele de pe obicetul 3D, acestea trebuie sa fie identice cu originalele) DONE
	//3. Teste pe cazuri reale DONE 
	//4. Extindere pe cazuri reale(fisier obj)
	//5. Optimizari timp constructie sistem
	//6. Simulare real-time pe fluxul de date primite de la o sursa

	return 0;
}