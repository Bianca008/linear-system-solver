#pragma once
#include <cstdint>
#include <vector>

class Matrix
{
public:
	Matrix() = default;
	Matrix(const std::vector<double>& values, size_t rows, size_t cols);
	~Matrix() = default;

	std::vector<double> GetData() const;
	size_t GetCols() const;
	size_t GetRows() const;
	double& at(size_t indexRow, size_t indexColumn);

	void Print();

private:
	std::vector<double> m_values;
	size_t m_cols;
	size_t m_rows;
};

