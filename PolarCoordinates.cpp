#include "PolarCoordinates.h"
#include "Point3D.h"

#include <cmath>

std::tuple<double, double, double> PolarCoordinates::GetPolarCoordinates(const Point3D& point)
{
	//r, theta, phi
	std::tuple<double, double, double> coordinates;
	std::get<0>(coordinates) = sqrt(std::pow(point.x, 2) + std::pow(point.y, 2) + std::pow(point.z, 2));
	std::get<1>(coordinates) = acos(point.z / std::get<0>(coordinates));
	std::get<2>(coordinates) = atan2(point.y, point.x);

	return coordinates;
}
