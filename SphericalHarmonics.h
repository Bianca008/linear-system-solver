#pragma once
#include <cstdint>
#include <complex>
#include <vector>

class Point3D;

class SphericalHarmonics
{
public:
	double GetRe(int l, int m, double theta, double phi) const;
	double GetIm(int l, int m, double theta, double phi) const;

	std::vector<std::complex<double>> ComputeSystemMatrix(const std::vector<std::tuple<double, double, double>>& pointsInPolarCoordinates,
		uint16_t lmax);
	std::vector<std::complex<double>> ComputeSystemVector(const std::vector<std::tuple<double, double, double>>& pointsInPolarCoordinates,
		uint16_t lmax) const;
	std::vector<double> RealSystemMatrix(std::vector<std::complex<double>> v, size_t lmax) const;
	std::vector<double> RealSystemVector(std::vector<std::complex<double>> v) const;
	std::complex<double> GetComplexCoeff(int l, int m, double theta, double phi) const;

private:
	std::complex<double> ComputeSystemMatrixElement(const std::vector<std::tuple<double, double, double>>& pointsInPolarCoordinates,
		int p,
		int q,
		int l,
		int m);
	std::complex<double> ComputeSystemVectorElement(const std::vector<std::tuple<double, double, double>>& pointsInPolarCoordinates,
		int posVec,
		int l,
		int m) const;
	double GetCoeff(int l, int m, double theta) const;

private:
	std::vector<std::vector<std::complex<double>>> m_commonMatrix;

};

