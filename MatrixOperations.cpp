#include "Matrix.h"
#include "MatrixOperations.h"

#include "mkl.h"

Matrix MatrixOperations::MultiplyMatrix(const Matrix& first, bool transposeFirst, const Matrix& second, bool transposeSecond) const
{
	/*
	m = nr of rows in firstMatrix and nr of rows in result matrix
	n = nr of cols in secondMatrix and nr of cols in result matrix
	k = nr of cols in firstMatrix and nr of rows in second matrix
	*/

	size_t m = 0, n = 0, k = 0;
	/*
	Leading dimension of first matrix = nr of cols in first matrix
	and leading dimension of second matrix = nr of cols in second matrix.
	More information here:
	https://software.intel.com/en-us/mkl-developer-reference-c-cblas-gemm
	*/
	size_t ldFirst = 0, ldSecond = 0;

	if (transposeFirst == false && transposeSecond == false)
	{
		m = first.GetRows();
		n = second.GetCols();
		k = first.GetCols();
		ldFirst = k;
		ldSecond = n;
	}
	if (transposeFirst == false && transposeSecond == true)
	{
		m = first.GetRows();
		n = second.GetRows();
		k = first.GetCols();
		ldFirst = k;
		ldSecond = k;
	}
	if (transposeFirst == true && transposeSecond == false)
	{
		m = first.GetCols();
		n = second.GetCols();
		k = first.GetRows();
		ldFirst = m;
		ldSecond = n;
	}
	if (transposeFirst == true && transposeSecond == true)
	{
		m = first.GetCols();
		n = second.GetRows();
		k = first.GetRows();
		ldFirst = m;
		ldSecond = k;
	}

	double* result;
	result = (double*)mkl_malloc(m * n * sizeof(double), 64);

	const auto matrixMajorTypeFirst = transposeFirst ? CblasTrans : CblasNoTrans;
	const auto matrixMajorType = transposeSecond ? CblasTrans : CblasNoTrans;

	std::vector<double> firstValues = first.GetData();
	double* firstVals = firstValues.data();

	std::vector<double> secondValues = second.GetData();
	double* secondVals = secondValues.data();

	cblas_dgemm(CblasRowMajor, matrixMajorTypeFirst, matrixMajorType,
		m, n, k, 1.0, firstVals, ldFirst, secondVals, ldSecond, 0.0, result, n);

	size_t numberOfIterations = m * n;

	std::vector<double> vals(result, result + numberOfIterations);

	Matrix resMat(vals, m, n);
	mkl_free(result);
	return resMat;
}

Matrix MatrixOperations::SolveSystem(const Matrix& matrix, const Matrix& vector) const
{
	std::vector<double> matrixValues = matrix.GetData();
	double* matrixVals = matrixValues.data();

	std::vector<double> vectorValues = vector.GetData();
	double* vectorVals = vectorValues.data();

	double wkopt;
	int lwork = -1;
	int nrhs = 1;
	int n = matrix.GetRows();
	int info;
	int ldb = n;
	int* ipiv = new int[n];

	double* work = (double*)malloc(n * sizeof(double));

	info = LAPACKE_dgesv(LAPACK_ROW_MAJOR, n, nrhs, matrixVals, n, ipiv, vectorVals, nrhs);

	if (info > 0) {
		printf("The diagonal element of the triangular factor of A,\n");
		printf("U(%i,%i) is zero, so that A is singular;\n", info, info);
		printf("the solution could not be computed.\n");
		exit(1);
	}

	std::vector<double> result(vectorVals, vectorVals + n);

	delete[] ipiv;
	free(work);

	return Matrix(result, matrix.GetRows(), 1);
}
