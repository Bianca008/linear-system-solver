#include "TransformMatrixIntoBinaryFile.h"
#include "Matrix.h"

#include <fstream>

void TransformMatrixIntoBinaryFile::TranformIntoBinaryFile(const Matrix& matrix, const std::string& file)
{
	std::ofstream out(file, std::ios::out | std::ios::binary);

	std::vector<double> matValues = matrix.GetData();
	double* intermediarMatValues = matValues.data();

	size_t rows = matrix.GetRows();
	out.write((char*)&rows, sizeof(int));

	size_t cols = matrix.GetCols();
	out.write((char*)&cols, sizeof(int));

	for (size_t index = 0; index < rows * cols; ++index)
		out.write((char*)&intermediarMatValues[index], sizeof(double));

	out.close();
}
