#include "Point3D.h"

#include <cmath>

Point3D::Point3D(double x, double y, double z) : x(x), y(y), z(z)
{
}

double Point3D::GetNorm() const
{
	return sqrt(std::pow(x, 2) + std::pow(y, 2) + std::pow(z, 2));
}
