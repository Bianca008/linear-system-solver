#pragma once
#include <string>

class Matrix;

class TransformMatrixIntoBinaryFile
{
public:
	static void TranformIntoBinaryFile(const Matrix& matrix, const std::string& file);
};

