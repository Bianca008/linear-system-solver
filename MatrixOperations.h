#pragma once
#include <cstdint>

class Matrix;

class MatrixOperations
{
public:
	MatrixOperations() = default;
	~MatrixOperations() = default;

	Matrix MultiplyMatrix(const Matrix & first,
		bool transposeFirst,
		const Matrix & second,
		bool transposeSecond) const;

	Matrix SolveSystem(const Matrix& matrix,
		const Matrix& vector) const;
};

