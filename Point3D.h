#pragma once

class Point3D
{
public:
	Point3D(double x, double y, double z);

	double GetNorm() const;

public:
	double x;
	double y;
	double z;
};

