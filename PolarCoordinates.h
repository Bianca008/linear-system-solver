#pragma once
#include <tuple>

class Point3D;

class PolarCoordinates
{
public:
	static std::tuple<double, double, double> GetPolarCoordinates(const Point3D& point);
};

